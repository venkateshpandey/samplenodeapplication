var rp= require('request-promise');
var options={
    methode: 'GET',    
    uri:'https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia/all-access/user/Africa/daily/2017042700/2018051700',
    json:true    
};

var data=[];
rp(options)
    .then(function(parseBody){    
    for(i=0 ;i<parseBody.items.length;i++){
       data.push({ Time: parseBody.items[i].timestamp, Views: parseBody.items[i].views});
    }
    })
    .catch(function (err){
        console.log("Error during processing: "+err);
});

const mongo = require('mongodb').MongoClient;
const url = 'mongodb://testuser1:testuser1@mongodb-860-0.cloudclusters.net/SampleNodeApplication?authSource=admin';

mongo.connect(url, function(err, database) {
    if (err) throw err;
    var db=database.db('SampleNodeApplication');
    var collections=db.collection('WikiData');
    collections.insertMany(data, function (err, result) {
        if (err) throw err;
        console.log("Total documents persisted: " + result.insertedCount);
        db.close();
    });
}); 